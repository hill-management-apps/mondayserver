using Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MondayServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string apiToken = "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjkwNTQxMTcyLCJ1aWQiOjU0NDQ2MzYsImlhZCI6IjIwMjAtMTEtMTBUMjA6MTA6MzUuMDAwWiIsInBlciI6Im1lOndyaXRlIn0.g1-u6RjDSE5cV8In5E8iXtNw_6J_kOTMHBVI0bK2Y6M";
            string apiRoot = "https://api.monday.com/v2/";
            
            // using statement used here to dispose of the client (but it's recommended to reuse HttpClient as much as possible in a real situation)
            // see https://blogs.msdn.microsoft.com/shacorn/2016/10/21/best-practices-for-using-httpclient-on-services/
            using (MondayClient client = new MondayClient(apiToken, apiRoot)) 
            {
                MondayService service = new MondayService(client);
                
                // get all boards
                List<Board> boards = service.GetBoards();
                
                Console.WriteLine("-- Boards --");
                //boards.ForEach(x => Console.WriteLine($"Board: {x.Name}"));

                // Make a collection of any board with Status=Ticket.
                List<Board> boardsWithTickets = new List<Board>();


                //if (boards.Any())
                for(int i = 0; i < boards.Count; i++)
                {
                    // get items for the first board in the list
                    Board board = service.GetBoardWithItems(boards[i].Id);
                    //Console.WriteLine($"\n-- Board {boards[i].Name} \nItems --");
                    foreach (var boardItem in board.Items)
                    {
                        //Console.WriteLine($"-- {boardItem.Id} {boardItem.Name}");

                        // THIS IS THE LINK WE WILL WRITE INTO FRESHSERVICE...
                        //string lnk = string.Format("https://ithill.monday.com/boards/{0}/pulses/{1}", board.Id, boardItem.Id);

                        if (board.Name == "Marty's Test Board")
                        {
                            Console.WriteLine($"\n-- Board {boards[i].Name} \nItems --");
                            foreach (var col in boardItem.ItemColumnValues)
                            {
                                Console.WriteLine(col.Title + "   -   " + col.Value + "  -  " + col.Text);
                            }
                        }

                        //if(boardItem.ItemColumnValues)
                        //service.ChangeTextColumnValue(boards[0].Id, board.Items[0].Id, "text3", "hello world!");
                    }



                    // update a column value with mutation
                    //Console.WriteLine($"\n-- Changing a column value --");
                    //var columnChange = service.ChangeTextColumnValue(boards[0].Id, board.Items[0].Id, "text3", "hello world!");
                    //Console.WriteLine($"The response body was: {columnChange}");
                }
            }
            Console.Read();
        }
    }
}

