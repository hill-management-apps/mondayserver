﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Entities
{
    public class Board
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "items")]
        public IList<Item> Items { get; set; }

    }
}