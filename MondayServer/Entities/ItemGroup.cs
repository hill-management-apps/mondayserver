﻿using Newtonsoft.Json;

namespace Entities
{
    public class ItemGroup
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}