﻿using Newtonsoft.Json;

namespace Entities
{
    public class ItemColumnValue
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "value")]
        public dynamic Value { get; set; }

        [JsonProperty(PropertyName = "text")]
        public dynamic Text { get; set; }
    }
}